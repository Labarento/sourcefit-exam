# Sourcefit Exam - User time logs

## Tests Results (Unit + Feature)

![](testresult.png)

## Api flow

### Register

#### Endpoint
POST /api/register

#### Payload
| Name        | Required |
|-------------|----------|
| first_name  | Yes      |
| last_name   | Yes      |
| middle_name | No       |
| contact_no  | No       |
| email       | Yes      |

### Response
```json
{
    "first_name": "John",
    "last_name": "Doe",
    "middle_name": "Mason",
    "email": "johndoe@gmail.com",
    "contact_no": "12345667",
    "updated_at": "2022-08-22T04:25:47.000000Z",
    "created_at": "2022-08-22T04:25:47.000000Z",
    "id": 4
}
```
----

### Time in

#### Endpoint
POST /api/timein

#### Payload
| Name    | Required |
|---------|----------|
| timein  | Yes      |
| user_id | Yes      |

### Response
```json
{
    "id": 7,
    "date_time_in": "2022-02-02T09:00:00.000000Z",
    "date_time_out": null,
    "logged_minutes": 0,
    "created_at": "2022-08-22T04:27:48.000000Z",
    "updated_at": "2022-08-22T04:27:48.000000Z",
    "user_id": 6
}
```

----

### Time out

#### Endpoint
POST /api/timeout

#### Payload
| Name       | Required |
|------------|----------|
| timeout    | Yes      |
| timelog_id | Yes      |

### Response
```json
{
    "id": 8,
    "date_time_in": "2022-02-02T14:00:00.000000Z",
    "date_time_out": "2022-02-02T22:00:00.000000Z",
    "logged_minutes": 480,
    "created_at": "2022-08-22T01:52:33.000000Z",
    "updated_at": "2022-08-22T01:52:42.000000Z",
    "user_id": 1
}
```

----

### Time logs
Time logs are grouped by date and will calculate accurate work hours based on multiple time logs.

#### Endpoint
GET /api/timelogs

### Response
```json
[
    {
        "date": "2022-02-02",
        "name": "Doe, John Mason",
        "minutes": 600,
        "time_in": "2022-02-02T09:00:00Z",
        "time_out": "2022-02-02T19:00:00Z",
        "logged_hours": 10,
        "hours_undertime": 0,
        "hours_worked": 8,
        "hours_overtime": 1,
        "hours_late": 0
    }
]
```

----

### Time sheets
Time sheets are all time logs wherein a day can have multiple time in and time out. eg. taking an emergency break out of work hours

#### Endpoint
GET /api/timesheets

### Response
```json
[
    {
        "id": 1,
        "date_time_in": "2022-01-30T09:00:00.000000Z",
        "date_time_out": "2022-01-30T13:00:00.000000Z",
        "logged_minutes": 240,
        "created_at": "2022-08-22T01:42:37.000000Z",
        "updated_at": "2022-08-22T01:43:01.000000Z",
        "user_id": 1
    },
    {
        "id": 2,
        "date_time_in": "2022-01-30T14:00:00.000000Z",
        "date_time_out": "2022-01-30T18:00:00.000000Z",
        "logged_minutes": 240,
        "created_at": "2022-08-22T01:43:33.000000Z",
        "updated_at": "2022-08-22T01:43:42.000000Z",
        "user_id": 1
    }
]
```
