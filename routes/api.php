<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TimelogController;
use App\Http\Controllers\RegisterController;

Route::post('register', RegisterController::class);
Route::post('timein', [TimelogController::class, 'timein']);
Route::post('timeout', [TimelogController::class, 'timeout']);
Route::get('timelogs', [TimelogController::class, 'timelogs']);
Route::get('timesheets', [TimelogController::class, 'timesheets']);
