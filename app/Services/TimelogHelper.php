<?php
declare(strict_types=1);

namespace App\Services;

use Carbon\Carbon;
use App\Services\Timelog\Interfaces\TimelogFactoryInterface;

final class TimelogHelper
{
    public static function getCalculatedHours($dateTimeIn, $dateTimeout = null): array
    {
        $timeIn = new Carbon($dateTimeIn);
        $fields = [
            'date_time_in' => $timeIn,
            'hrs_worked' => 0,
            'hrs_late' => 0,
            'hrs_overtime' => 0,
            'hrs_undertime' => 0,
            'logged_minutes' => 0,
        ];

        if ($dateTimeout === null) {
            return $fields;
        }

        $timeOut = new Carbon($dateTimeout);

        if ($dateTimeout !== null) {
            $fields['date_time_out'] = $timeOut;
        }

        $loggedMinutes = $timeOut->diffInMinutes($timeIn);
        $totalOfficeHours = TimelogFactoryInterface::REGULAR_HOUR + TimelogFactoryInterface::BREAK_HOUR;
        $totalOfficeHourDiff = ($loggedMinutes / 60) - $totalOfficeHours;
        $fields['hrs_worked'] = TimelogFactoryInterface::REGULAR_HOUR;

        // Resolve hours worked.
        if ($totalOfficeHourDiff >= 0) {
            // Calculate overtime hours.
            if ($totalOfficeHourDiff >= 1) {
                $fields['hrs_overtime'] = $totalOfficeHourDiff;
            }
        }

        // Calculate undertime hours.
        if ($totalOfficeHourDiff <= -1) {
            $fields['hrs_undertime'] = \abs($totalOfficeHourDiff);
        }

        $fields['logged_minutes'] = $loggedMinutes;
        $fields['hrs_worked'] = \round(self::resolveHoursWorked($loggedMinutes), 2);

        return $fields;
    }

    public static function resolveHoursWorked(float $loggedMinutes)
    {
        $totalOfficeHours = TimelogFactoryInterface::REGULAR_HOUR + TimelogFactoryInterface::BREAK_HOUR;
        $workDiff = ($loggedMinutes / 60) <=> $totalOfficeHours;
        $totalOfficeHourDiff = ($loggedMinutes / 60) - $totalOfficeHours;

        if ($workDiff === 0 || $workDiff === 1) {
            return TimelogFactoryInterface::REGULAR_HOUR;
        }

        return TimelogFactoryInterface::REGULAR_HOUR + $totalOfficeHourDiff;
    }
}
