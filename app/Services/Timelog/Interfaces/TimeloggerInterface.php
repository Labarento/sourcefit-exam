<?php
declare(strict_types=1);

namespace App\Services\Timelog\Interfaces;

interface TimeloggerInterface
{
    public function timein(int $userId, ?string $dateTimein = null): TimelogInterface;

    public function timeout(int $timelogId, ?string $dateTimeout = null): TimelogInterface;
}
