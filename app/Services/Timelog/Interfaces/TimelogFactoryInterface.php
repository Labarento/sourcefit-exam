<?php
declare(strict_types=1);

namespace App\Services\Timelog\Interfaces;

interface TimelogFactoryInterface
{
    /**
     * @var int
     */
    public const REGULAR_HOUR = 8;

    /**
     * @var int
     */
    public const BREAK_HOUR = 1;

    /**
     * @var int
     */
    public const TOTAL_OFFICE_HOURS = 9;

    public function create(int $userId, string $dateTimeIn, ?string $dateTimeout = null): TimelogInterface;
}
