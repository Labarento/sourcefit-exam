<?php
declare(strict_types=1);

namespace App\Services\Timelog\Interfaces;

use DateTime;

interface TimelogInterface
{
    public function getDate(): ?DateTime;

    public function getLoggedMinutes(): ?int;

    public function getTimeIn(): ?DateTime;

    public function getTimeout(): ?DateTime;

    public function getUserId(): ?int;
}
