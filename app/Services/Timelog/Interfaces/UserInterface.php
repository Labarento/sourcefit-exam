<?php
declare(strict_types=1);

namespace App\Services\Timelog\Interfaces;

interface UserInterface
{
    public function getUserId(): ?int;

    public function getFirstName(): ?string;

    public function getLastName(): ?string;

    public function getFullname(): ?string;

    public function getContactNo(): ?string;

    public function getEmail(): ?string;
}
