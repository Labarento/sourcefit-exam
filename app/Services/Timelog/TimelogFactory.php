<?php
declare(strict_types=1);

namespace App\Services\Timelog;

use Carbon\Carbon;
use App\Models\Timelog;
use App\Services\TimelogHelper;
use App\Services\Timelog\Interfaces\TimelogInterface;
use App\Services\Timelog\Interfaces\TimelogFactoryInterface;
use App\Services\Repositories\Interfaces\UserRepositoryInterface;

final class TimelogFactory implements TimelogFactoryInterface
{
    /**
     * @var \App\Services\Repositories\Interfaces\UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create(int $userId, string $dateTimeIn, ?string $dateTimeout = null): TimelogInterface
    {
        /** @var \App\Services\Timelog\Interfaces\UserInterface $user */
        $user = $this->userRepository->findOrFail($userId);

        /** @var \Illuminate\Database\Eloquent\Model|\App\Services\Timelog\Interfaces\TimelogInterface $timelog */
        $timelog = new Timelog();

        $timelog->setUser($user);

        $timelog->fill(TimelogHelper::getCalculatedHours($dateTimeIn, $dateTimeout));

        return $timelog;
    }
}
