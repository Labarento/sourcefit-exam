<?php
declare(strict_types=1);

namespace App\Services\Timelog;

use App\Models\Timelog;
use Ramsey\Uuid\Type\Time;
use App\Services\Timelog\Interfaces\TimelogInterface;
use App\Services\Timelog\Interfaces\TimeloggerInterface;
use App\Services\Timelog\Interfaces\TimelogFactoryInterface;
use App\Services\Repositories\Interfaces\TimelogRepositoryInterface;

final class Timelogger implements TimeloggerInterface
{
    /**
     * @var \App\Services\Timelog\Interfaces\TimelogFactoryInterface
     */
    private $timelogFactory;

    /**
     * @var \App\Services\Repositories\Interfaces\TimelogRepositoryInterface
     */
    private $timelogRepository;

    public function __construct(TimelogFactoryInterface $timelogFactory, TimelogRepositoryInterface $timelogRepository)
    {
        $this->timelogFactory = $timelogFactory;
        $this->timelogRepository = $timelogRepository;
    }

    public function timein(int $userId, ?string $dateTimein = null): TimelogInterface
    {
        $timelog = $this->timelogFactory->create($userId, $dateTimein ?? new \DateTime());

        return $this->timelogRepository->create($timelog);
    }

    public function timeout(int $timelogId, ?string $dateTimeout = null): TimelogInterface
    {
        return $this->timelogRepository->update($timelogId, ['date_time_out' => $dateTimeout ?? new \DateTime()]);
    }
}
