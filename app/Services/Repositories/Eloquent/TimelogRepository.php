<?php
declare(strict_types=1);

namespace App\Services\Repositories\Eloquent;

use App\Models\Timelog;
use App\Services\TimelogHelper;
use Illuminate\Support\Facades\DB;
use App\Services\Timelog\Interfaces\TimelogInterface;
use App\Services\Timelog\Interfaces\TimelogFactoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use App\Services\Repositories\Interfaces\TimelogRepositoryInterface;

final class TimelogRepository implements TimelogRepositoryInterface
{
    /**
     * @var \App\Services\Timelog\Interfaces\TimelogFactoryInterface
     */
    private $timelogFactory;

    public function __construct(TimelogFactoryInterface $timelogFactory)
    {
        $this->timelogFactory = $timelogFactory;
    }

    /**
     * @param mixed[]|\App\Models\Timelog $data
     *
     * @return \App\Services\Timelog\Interfaces\TimelogInterface
     */
    public function create($data): TimelogInterface
    {
        $timelog = null;

        if (\is_array($data)) {
            $timelog = new Timelog($data);

            $timelog->save();

            return $timelog;
        }

        if ($data instanceof TimelogInterface) {
            $data->save();

            $timelog = $data->refresh();
        }

        return $timelog;
    }

    /**
     * Get all user timesheet.
     *
     * @return \App\Models\Timelog[]
     */
    public function all(): array
    {
        return Timelog::all()->toArray();
    }

    public function findOrFail(int $identifier)
    {
        $timelog = Timelog::find($identifier);

        if ($timelog === null) {
            throw new NotFoundHttpException(\sprintf('Timelog not found with id [%s]', $identifier));
        }

        return $timelog;
    }

    public function update(int $timelogId, array $data): TimelogInterface
    {
        /** @var \App\Services\Timelog\Interfaces\TimelogInterface|\Illuminate\Database\Eloquent\Model $timelog */
        $timelog = $this->findOrFail($timelogId);

        if ($timelog->getTimeout() !== null) {
            throw new ConflictHttpException('User has already been timed off.');
        }

        $timelog->fill($data);

        $loggedMinutes = TimelogHelper::getCalculatedHours(
            $timelog->getTimeIn(),
            $timelog->getTimeout()
        )['logged_minutes'] ?? 0;

        $timelog->fill(['logged_minutes' => $loggedMinutes]);

        $timelog->save();

        return $timelog->refresh();
    }

    /**
     * Get all user time logs grouped by daily.
     *
     * @return mixed[]
     */
    public function allUserTimelogs(): array
    {
        $result = DB::table('time_logs')
            ->select(DB::raw(
                "users.id as user_id,
                DATE_FORMAT(MIN(date_time_in), '%Y-%m-%dT%H:%i:%sZ') as time_in,
                DATE_FORMAT(MAX(date_time_out), '%Y-%m-%dT%H:%i:%sZ') as time_out,
                CONCAT(users.last_name, ', ', users.first_name, ' ', users.middle_name) as name,
                CAST(date_time_in AS DATE) as date_fmt,
                SUM(logged_minutes) as minutes,
                ROUND((SUM(logged_minutes) / 60), 2) as logged_hours,
                (ROUND((SUM(logged_minutes) / 60), 2) - 9) as hours_diff"
            ))
            ->leftJoin('users', 'users.id', '=', 'time_logs.user_id')
            ->whereNotNull('date_time_out')->groupBy(DB::raw('user_id, date_fmt'));

        $timelogs = [];

        foreach ($result->get()->toArray() as $timelog) {
            $overtime = ($timelog->logged_hours - TimelogFactoryInterface::TOTAL_OFFICE_HOURS);
            $regularHours = TimelogFactoryInterface::REGULAR_HOUR;

            $timelogs[] =[
                'date' => $timelog->date_fmt,
                'name' => $timelog->name,
                'minutes' => $timelog->minutes,
                'time_in' => $timelog->time_in,
                'time_out' => $timelog->time_out,
                'logged_hours' => $timelog->logged_hours,
                'hours_undertime' => $timelog->hours_diff < 0 ? \abs($timelog->hours_diff) : 0,
                'hours_worked' => $timelog->hours_diff >= 0 ? $regularHours : ($regularHours - \abs($timelog->hours_diff)),
                'hours_overtime' => $overtime > 0 ? $overtime : 0,

                // For now this is always 0, we don't have a criteria what time we consider late. eg. 9am onwards
                'hours_late' => 0,
            ];
        }

       return $timelogs;
    }
}
