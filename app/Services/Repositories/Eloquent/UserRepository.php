<?php
declare(strict_types=1);

namespace App\Services\Repositories\Eloquent;

use App\Models\User;
use App\Services\Timelog\Interfaces\UserInterface;
use App\Services\Repositories\Interfaces\UserRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class UserRepository implements UserRepositoryInterface
{
    public function all(): array
    {
        return User::all();
    }

    public function findOrFail(int $identifier): UserInterface
    {
        $user = User::find($identifier);

        if ($user === null) {
            throw new NotFoundHttpException(\sprintf('User not found with id [%s]', $identifier));
        }

        return $user;
    }

    public function getTimelogs(int $userId): array
    {
        /** @var \App\Models\User $user */
        $user = $this->findOrFail($userId);

        return $user->timelogs()->getResults();
    }
}
