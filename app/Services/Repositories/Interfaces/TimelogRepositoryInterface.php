<?php
declare(strict_types=1);

namespace App\Services\Repositories\Interfaces;

use App\Services\Timelog\Interfaces\TimelogInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface TimelogRepositoryInterface extends AppRepositoryInterface
{
    public function create($data): TimelogInterface;

    public function update(int $timelogId, array $data): TimelogInterface;

    public function allUserTimelogs(): array;
}
