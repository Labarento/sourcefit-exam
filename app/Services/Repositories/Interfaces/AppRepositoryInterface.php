<?php
declare(strict_types=1);

namespace App\Services\Repositories\Interfaces;

interface AppRepositoryInterface
{
    public function all(): array;

    public function findOrFail(int $identifier);
}
