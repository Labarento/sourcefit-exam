<?php
declare(strict_types=1);

namespace App\Services\Repositories\Interfaces;

interface UserRepositoryInterface extends AppRepositoryInterface
{
    /**
     * @return \App\Services\Timelog\Interfaces\TimelogInterface[]
     */
    public function getTimelogs(int $userId): array;
}
