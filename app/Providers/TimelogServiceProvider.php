<?php
declare(strict_types=1);

namespace App\Providers;

use App\Services\Timelog\Timelogger;
use Illuminate\Support\ServiceProvider;
use App\Services\Timelog\TimelogFactory;
use App\Services\Repositories\Eloquent\UserRepository;
use App\Services\Timelog\Interfaces\TimeloggerInterface;
use App\Services\Repositories\Eloquent\TimelogRepository;
use App\Services\Timelog\Interfaces\TimelogFactoryInterface;
use App\Services\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Repositories\Interfaces\TimelogRepositoryInterface;

final class TimelogServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(TimelogFactoryInterface::class, TimelogFactory::class);
        $this->app->bind(TimeloggerInterface::class, Timelogger::class);
    }
}
