<?php
declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Repositories\Eloquent\UserRepository;
use App\Services\Repositories\Eloquent\TimelogRepository;
use App\Services\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Repositories\Interfaces\TimelogRepositoryInterface;

final class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(TimelogRepositoryInterface::class, TimelogRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }
}
