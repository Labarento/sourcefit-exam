<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;

final class RegisterController
{
    public function __invoke(RegisterRequest $request): JsonResponse
    {
        $payload = $request->all();

        $user = new User([
            'first_name' => $payload['first_name'] ?? null,
            'last_name' => $payload['last_name'] ?? null,
            'middle_name' => $payload['middle_name'] ?? null,
            'email' => $payload['email'] ?? null,
            'contact_no' => $payload['contact_no'] ?? null,
        ]);

        $user->save();

        return response()->json($user);
    }
}
