<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TimeinRequest;
use App\Http\Requests\TimeoutRequest;
use App\Services\Timelog\Interfaces\TimeloggerInterface;
use App\Services\Repositories\Interfaces\TimelogRepositoryInterface;

final class TimelogController extends Controller
{
    /**
     * @var \App\Services\Repositories\Interfaces\TimelogRepositoryInterface
     */
    private $timelogRepository;

    /**
     * @var \App\Services\Timelog\Interfaces\TimeloggerInterface
     */
    private $timelogger;

    public function __construct(TimeloggerInterface $timelogger, TimelogRepositoryInterface $timelogRepository)
    {
        $this->timelogger = $timelogger;
        $this->timelogRepository = $timelogRepository;
    }

    public function timein(TimeinRequest $request): JsonResponse
    {
        $timein = $request->get('timein', null);
        $userId = $request->get('user_id');

        return response()->json($this->timelogger->timein((int)$userId, $timein));
    }

    public function timeout(TimeoutRequest $request): JsonResponse
    {
        $timeout = $request->get('timeout', null);
        $timelogId = $request->get('timelog_id');

        return response()->json($this->timelogger->timeout((int)$timelogId, $timeout));
    }

    /**
     * Time logs are grouped by date and will calculate accurate work hours based on multiple time logs.
     */
    public function timelogs(): JsonResponse
    {
        return response()->json($this->timelogRepository->allUserTimelogs());
    }

    /**
     * Time sheets are all time logs wherein a day can have multiple time in and time out. eg. taking an emergency break out of work hours
     */
    public function timesheets(): JsonResponse
    {
        return response()->json($this->timelogRepository->all());
    }
}
