<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TimeinRequest extends FormRequest
{
    public function rules()
    {
        return [
            'user_id' => 'required|int',
            'timein' => 'nullable|date',
        ];
    }
}
