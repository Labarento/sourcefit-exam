<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TimeoutRequest extends FormRequest
{
    public function rules()
    {
        return [
            'timeout' => 'nullable|date',
            'timelog_id' => 'required|int',
        ];
    }
}
