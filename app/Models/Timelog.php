<?php
declare(strict_types=1);

namespace App\Models;

use DateTime;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Services\Timelog\Interfaces\UserInterface;
use App\Services\Timelog\Interfaces\TimelogInterface;

final class Timelog extends Model implements TimelogInterface
{
    protected $table = 'time_logs';
    protected $fillable = [
        'date_time_in',
        'date_time_out',
        'logged_minutes',
    ];

    protected $dates = [
        'date_time_in',
        'date_time_out',
    ];

    protected $casts = [
        'date_time_in' => 'datetime',
        'date_time_out' => 'datetime',
    ];

    public function getDate(): DateTime
    {
        return $this->created_at;
    }

    public function getTimeIn(): ?DateTime
    {
        return $this->date_time_in;
    }

    public function getTimeout(): ?DateTime
    {
        return $this->date_time_out;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUser(UserInterface $user): self
    {
        $this->user_id = $user->getUserId();

        return $this;
    }

    public function getLoggedMinutes(): ?int
    {
        return $this->logged_minutes;
    }
}
