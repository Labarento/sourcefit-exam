<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

final class RegisterControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testRegisterValidationFailed(): void
    {
        $response = $this->postJson('/api/register', []);

        $response->assertJsonStructure(['message', 'errors']);
        $response->assertUnprocessable();
    }

    public function testRegisterSuccessful(): void
    {
        $payload = [
            'first_name' => 'John',
            'middle_name' => 'Mason',
            'last_name' => 'Doe',
            'contact_no' => '123456',
            'email' => 'johndoe@me.com',
        ];

        $response = $this->postJson('/api/register', $payload);

        $response->assertJsonStructure([
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'email',
            'contact_no',
            'updated_at',
            'created_at'
        ]);
        $response->assertJsonFragment($payload);
        $response->assertSuccessful();
    }
}
