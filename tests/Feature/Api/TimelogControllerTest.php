<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Services\Timelog\Interfaces\TimelogFactoryInterface;

final class TimelogControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testTimeInAndTimeOutSuccessful(): void
    {
        $timein = '2022-01-29 09:00';
        $user = User::factory()->create();
        $payload = [
            'timein' => $timein,
            'user_id' => $user->id
        ];

        $response = $this->postJson('/api/timein', $payload);

        $response->assertJsonStructure([
            'id',
            'date_time_in',
            'date_time_out',
            'logged_minutes',
            'user_id',
            'created_at',
            'updated_at',
        ]);
        $response->assertJsonFragment([
            'date_time_in' => '2022-01-29T09:00:00.000000Z',
            'logged_minutes' => 0,
            'user_id' => $user->id,
        ]);
        $response->assertSuccessful();

        $timelog = $response->getData();

        $payload = [
            'timelog_id' => $timelog->id,
            'timeout' => '2022-01-29 18:00:00',
            'user_id' => $user->id,
        ];

        $response = $this->postJson('/api/timeout', $payload);

        $response->assertJsonStructure([
            'id',
            'date_time_in',
            'date_time_out',
            'logged_minutes',
            'user_id',
            'created_at',
            'updated_at',
        ]);
        $response->assertJsonFragment([
            'date_time_in' => '2022-01-29T09:00:00.000000Z',
            'date_time_out' => '2022-01-29T18:00:00.000000Z',
            'logged_minutes' => 540,
            'user_id' => $user->id,
        ]);
    }

    public function testTimeInValidationFailedTest(): void
    {
        $response = $this->postJson('/api/timein', []);

        $response->assertJsonStructure(['message', 'errors']);
        $response->assertUnprocessable();
    }

    public function testTimeOutValidationFailedTest(): void
    {
        $response = $this->postJson('/api/timeout', []);

        $response->assertJsonStructure(['message', 'errors']);
        $response->assertUnprocessable();
    }

    public function testTimelogs(): void
    {
        [$timelog1, $timelog2, $timelog3, $timelogMulti] = $this->createTimelogs();

        $response = $this->getJson('/api/timelogs');

        $response->assertJsonCount(4);
        $response->assertSuccessful();

        foreach ($response->getData() as $assertTimelog) {
            if ($assertTimelog->date === $timelog1['date']) {
                $this->assertTimelog($timelog1, $assertTimelog);
            }

            if ($assertTimelog->date === $timelog2['date']) {
                $this->assertTimelog($timelog2, $assertTimelog);
            }

            if ($assertTimelog->date === $timelog3['date']) {
                $this->assertTimelog($timelog3, $assertTimelog);
            }

            if ($assertTimelog->date === $timelogMulti['date']) {
                $this->assertTimelog($timelogMulti, $assertTimelog);
            }
        }
    }

    private function assertTimelog($expectedTimelog, $assertTimelog): void
    {
        self::assertEquals($expectedTimelog['minutes'], $assertTimelog->minutes, 'Minutes');
        self::assertEquals($expectedTimelog['logged_hours'], $assertTimelog->logged_hours, 'Logged hours');
        self::assertEquals($expectedTimelog['hours_undertime'], $assertTimelog->hours_undertime, 'Undertime');
        self::assertEquals($expectedTimelog['hours_worked'], $assertTimelog->hours_worked, 'Worked');
        self::assertEquals($expectedTimelog['hours_overtime'], $assertTimelog->hours_overtime, 'Overtime');
        self::assertEquals($expectedTimelog['date'], $assertTimelog->date, 'Date');
    }

    private function createTimelogs(): array
    {
        $user = User::factory()->create();

        /** @var \App\Services\Timelog\Interfaces\TimelogFactoryInterface $factory */
        $factory = $this->app->get(TimelogFactoryInterface::class);

        // User timelog with 8 hours of work
        $timelog1 = $factory->create($user->id, '2022-01-29 09:00:00', '2022-01-29 18:00:00');
        // User timelog with 10 hours of work OVERTIME
        $timelog2 = $factory->create($user->id, '2022-01-30 09:00:00', '2022-01-30 19:00:00');
        // User timelog with 7hrs of work UNDERTIME
        $timelog3 = $factory->create($user->id, '2022-02-01 09:00:00', '2022-02-01 17:00:00');

        // Multiple time logs within the day
        $timeMulti1 = $factory->create($user->id, '2022-02-02 09:00:00', '2022-02-02 13:00:00');
        $timeMulti2 = $factory->create($user->id, '2022-02-02 14:00:00', '2022-02-02 19:00:00');

        $timelog1->save();
        $timelog2->save();
        $timelog3->save();
        $timeMulti1->save();
        $timeMulti2->save();

        return [
            [
                'minutes' => 540,
                'logged_hours' => 9,
                'hours_undertime' => 0,
                'hours_worked' => 8,
                'hours_overtime' => 0,
                'date' => '2022-01-29',
            ],
            [
                'minutes' => 600,
                'logged_hours' => 10,
                'hours_undertime' => 0,
                'hours_worked' => 8,
                'hours_overtime' => 1,
                'date' => '2022-01-30',
            ],
            [
                'minutes' => 480,
                'logged_hours' => 8,
                'hours_undertime' => 1,
                'hours_worked' => 7,
                'hours_overtime' => 0,
                'date' => '2022-02-01',
            ],
            [
                'minutes' => 540,
                'logged_hours' => 9,
                'hours_undertime' => 0,
                'hours_worked' => 8,
                'hours_overtime' => 0,
                'date' => '2022-02-02',
            ]
        ];
    }
}
