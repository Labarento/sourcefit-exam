<?php
declare(strict_types=1);

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Timelog;
use App\Services\Timelog\Timelogger;
use App\Services\Timelog\TimelogFactory;
use App\Services\Repositories\Eloquent\UserRepository;
use App\Services\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Repositories\Interfaces\TimelogRepositoryInterface;

/** @covers \App\Services\Timelog\Timelogger */
final class TimeloggerTest extends TestCase
{
    public function testTimein(): void
    {
        $user = new User();
        $user->setAttribute('id', 1);
        $timein = '2022-01-29 09:00';
        $timelog = new Timelog();

        $timelogRepository = $this->mock(TimelogRepositoryInterface::class);
        $timelogRepository
            ->shouldReceive('create')
            ->once()
            ->withArgs(function($timelog) use ($user) {
                self::assertEquals($timelog->user_id, $user->id);

                return true;
            })
            ->andReturn($timelog);

        $userRepository = $this->mock(UserRepositoryInterface::class);
        $userRepository
            ->shouldReceive('findOrFail')
            ->once()
            ->with($user->id)
            ->andReturn($user);

        $timelogger = new Timelogger(
            new TimelogFactory($userRepository),
            $timelogRepository
        );

        $assertTimelog = $timelogger->timein($user->id, $timein);

        self::assertSame($timelog, $assertTimelog);
    }

    public function testTimeout(): void
    {
        $user = new User();
        $user->setAttribute('id', 1);

        $timeout = '2022-01-29 17:00';
        $timelog = new Timelog();
        $timelog->setAttribute('id', 1);

        $timelogRepository = $this->mock(TimelogRepositoryInterface::class);
        $timelogRepository
            ->shouldReceive('update')
            ->once()
            ->withArgs(function($timelogId) use ($timelog) {
                self::assertEquals($timelog->id, $timelogId);

                return true;
            })
            ->andReturn($timelog);

        $timelogger = new Timelogger(
            new TimelogFactory(new UserRepository()),
            $timelogRepository
        );

        $assertTimelog = $timelogger->timeout($timelog->id, $timeout);

        self::assertSame($timelog, $assertTimelog);
    }
}
